### Functional Requirements ###
1. The ball should rebound off the sides, top of the game board and off the paddle. 
2. If the ball falls off the bottom of the board the game is over or a life is lost.
3. The game should play from the Unity Editor.
4. The game should be re-playable without closing the game.

### Extra Credit ###
1.	Can be played in ANY screen size
2.	A playable EXE in the “CerebralFixTest\Bin” folder
3.	Extra feature: Record Scores in the game
