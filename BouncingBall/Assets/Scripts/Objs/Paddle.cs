using UnityEngine;

public class Paddle : CollideBase
{
    public float move_speed = 4f;
    Vector2 direction_left = new Vector2(-1, 0);
    Vector2 direction_right = new Vector2(1, 0);

    float edge_left = 0;
    float edge_right = 0;
    bool allow_move = false;

    public void InitPos()
    {
        Vector2 my_pos = this.transform.localPosition;
        my_pos.x = 0;
        this.transform.localPosition = my_pos;
    }

    void Start()
    {
        Camera cam = Camera.main;
        float cam_height = 2f * cam.orthographicSize;
        float cam_width = cam_height * cam.aspect;
        float self_width = this.transform.localScale.y;

        edge_right = (cam_width - self_width) / 2;
        edge_left = -edge_right;
    }

    void Update()
    {
        if (!allow_move)
            return;

        if (Input.GetKey("a"))
            MovePaddle(direction_left * move_speed);
        if (Input.GetKey("d"))
            MovePaddle(direction_right * move_speed);
    }

    void MovePaddle(Vector2 speed)
    {
        Vector2 my_pos = this.transform.localPosition;
        my_pos += Time.deltaTime * speed;
        my_pos.x = Mathf.Clamp(my_pos.x, edge_left, edge_right);
        this.transform.localPosition = my_pos;
    }

    public void AllowMove(bool allowmove)
    {
        allow_move = allowmove;
    }
}