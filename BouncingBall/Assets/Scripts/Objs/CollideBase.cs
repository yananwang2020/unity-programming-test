using UnityEngine;

public class CollideBase : MonoBehaviour
{
    public enum BoardType
    {
        Left,
        Right,
        Top,
        Bottom,
        Paddle
    }

    public Vector2 speed_change_direction;
    public BoardType board_type;

    protected void OnCollisionEnter2D(Collision2D other)
    {
        if (board_type == BoardType.Bottom)
        {
            GlobalEventManager.Instance.CallAction(ActionName.GameEnd);
            return;
        }

        Debug.Log("on collisionEnter Ball");
        Ball ball = other.gameObject.GetComponent<Ball>();
        if (ball != null)
        {
            ball.ChangeDirection(speed_change_direction);
        }
    }

}
