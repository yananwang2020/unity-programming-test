using UnityEngine;

public class Board : CollideBase
{
    // Start is called before the first frame update
    void Awake()
    {
        AdjestPosScl();
    }

    void AdjestPosScl()
    {
        Camera cam = Camera.main;
        float height = 2f * cam.orthographicSize;
        float width = height * cam.aspect;
        
        Vector3 pos = Vector3.zero;
        Vector3 scl = new Vector3(0.1f, 1, 1);

        switch(board_type)
        {
            case BoardType.Left:
                pos.x = -width/2;
                scl.y = height;
                break;
            case BoardType.Right:
                pos.x = width/2;
                scl.y = height;
                break;
            case BoardType.Top:
                pos.y = height/2;
                scl.y = width;
                break;
            case BoardType.Bottom:
                pos.y = -height/2;
                scl.y = width;
                break;
        }

        this.transform.localPosition = pos;
        this.transform.localScale = scl;
    }
}
