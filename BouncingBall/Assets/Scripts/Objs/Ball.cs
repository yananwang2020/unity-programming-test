using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed = 2;
    Vector2 velocity;
    Rigidbody2D rigid_body;

    // Start is called before the first frame update
    void Awake()
    {
        float x = Random.Range(-0.5f, 0.5f);
        float y = Random.Range(-1f, 1f);
        Vector2 direction = new Vector2(x, y);
        velocity = direction.normalized * speed;

        rigid_body = GetComponent<Rigidbody2D>();
    }

    public void ChangeDirection(Vector2 direction)
    {
        velocity.x *= direction.x;
        velocity.y *= direction.y;

        rigid_body.velocity = velocity;
    }

    public void InitPos()
    {
        this.transform.localPosition = Vector2.zero;
    }

    public void AllowMove(bool isallow)
    {
        if (isallow)
            rigid_body.velocity = velocity;
        else
            rigid_body.velocity = Vector2.zero;

    }
}
