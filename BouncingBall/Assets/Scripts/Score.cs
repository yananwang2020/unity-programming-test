using UnityEngine;

public class Score : MonoBehaviour
{
    float survival_time = 0;
    bool allow_record = false;

    // Update is called once per frame
    void Update()
    {
        if(allow_record)
        {
            survival_time += Time.deltaTime;
        }
    }

    public void InitScore()
    {
        survival_time = 0;
        allow_record = false;
    }

    public void AllowRecord(bool isallow)
    {
        allow_record = isallow;
    }

    public int GetScore()
    {
        return Mathf.FloorToInt(survival_time);
    }
}
