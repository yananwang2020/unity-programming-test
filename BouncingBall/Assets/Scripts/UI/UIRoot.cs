using UnityEngine;
using UnityEngine.UI;

public class UIRoot : MonoBehaviour
{
    public GameObject panel_welcome;
    public GameObject panel_gaming;
    public GameObject panel_result;
    public Text text_score;
    public GameObject text_pause;
    
    public void OnBtnStartClick()
    {
        GlobalEventManager.Instance.CallAction(ActionName.GameStart);
    }

    public void OnBtnReturnClick()
    {
        GlobalEventManager.Instance.CallAction(ActionName.GameLobby);
    }

    public void ShowWelcomeUI(bool isshow)
    {
        panel_welcome.SetActive(isshow);
    }

    public void ShowGamingUI(bool isshow)
    {
        panel_gaming.SetActive(isshow);
    }

    public void ShowResultUI(bool isshow, int score)
    {
        text_score.text = score.ToString();
        panel_result.SetActive(isshow);
    }

    public void PauseGame(bool ispause)
    {
        text_pause.SetActive(ispause);
    }
}
