using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    public Text countdown_text;
    public int countdown_total;
    int countdown_crt;

    private void OnEnable()
    {
        countdown_crt = 0;
        StartCoroutine("StartCountDown");
    }

    IEnumerator StartCountDown()
    {
        Debug.Log("StartCountDown");
        while (countdown_crt < countdown_total)
        {
            countdown_text.text = (countdown_total - countdown_crt).ToString();
            yield return new WaitForSeconds(1);
            countdown_crt++;
        }

        countdown_text.text = "GO!";
        yield return new WaitForSeconds(0.2f);
        countdown_text.text = "";

    }
}
