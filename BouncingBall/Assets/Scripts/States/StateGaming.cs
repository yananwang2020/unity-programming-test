﻿using System.Collections;
using UnityEngine;

class StateGaming : State
{
    public StateGaming(GameSystem gs) : base(gs)
    {

    }

    bool is_count_down = false;
    bool is_game_pause = false;

    public override IEnumerator StateStart()
    {
        is_count_down = true;
        game_system.ui_root.ShowGamingUI(true);
        game_system.paddle.AllowMove(true);

        yield return new WaitForSeconds(3f);

        is_count_down = false;
        game_system.ball.AllowMove(true);
        game_system.score.AllowRecord(true);
    }

    public override IEnumerator StateEnd()
    {
        game_system.ui_root.ShowGamingUI(false);
        game_system.paddle.AllowMove(false);
        game_system.ball.AllowMove(false);
        game_system.score.AllowRecord(false);
        yield return null;
    }

    public override IEnumerator PauseGame()
    {
        // Don't pause when count down
        if (is_count_down)
            yield return null;

        is_game_pause = !is_game_pause;

        game_system.ui_root.PauseGame(is_game_pause);
        game_system.paddle.AllowMove(!is_game_pause);
        game_system.ball.AllowMove(!is_game_pause);
        game_system.score.AllowRecord(!is_game_pause);

        yield return null;
    }
}
