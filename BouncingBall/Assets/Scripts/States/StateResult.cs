﻿using System.Collections;

class StateResult : State
{
    public StateResult(GameSystem gs) : base(gs)
    {

    }
    
    public override IEnumerator StateStart()
    {
        int score = game_system.score.GetScore();
        game_system.ui_root.ShowResultUI(true, score);

        yield return null;
    }

    public override IEnumerator StateEnd()
    {
        game_system.ui_root.ShowResultUI(false, 0);
        yield return null;
    }
}
