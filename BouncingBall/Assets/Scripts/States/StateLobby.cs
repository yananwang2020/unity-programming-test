﻿using System.Collections;

class StateLobby : State
{
    public StateLobby(GameSystem gs):base(gs)
    {

    }

    public override IEnumerator StateStart()
    {
        game_system.ball.InitPos();
        game_system.paddle.InitPos();
        game_system.score.InitScore();
        game_system.ui_root.ShowWelcomeUI(true);

        yield return null;
    }

    public override IEnumerator StateEnd()
    {
        game_system.ui_root.ShowWelcomeUI(false);
        return base.StateEnd();
    }
}
