﻿using System;
using UnityEngine;

public class GameSystem : StateMachine
{
    public UIRoot ui_root;
    public Paddle paddle;
    public Ball ball;
    public Score score;

    private void Start() 
    {
        RegistActions();
        EnterLobby();
    }

    private void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            StartCoroutine(crt_state.PauseGame());
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    public void RegistActions()
    {
        GlobalEventManager.Instance.RegisterAction(ActionName.GameLobby, new Action(EnterLobby));
        GlobalEventManager.Instance.RegisterAction(ActionName.GameStart, new Action(StartGame));
        GlobalEventManager.Instance.RegisterAction(ActionName.GameEnd, new Action(ShowResult));
    }

    public void EnterLobby()
    {
        SetState(new StateLobby(this));
    }

    public void StartGame()
    {        
        SetState(new StateGaming(this));
    }

    public void ShowResult()
    {
        SetState(new StateResult(this));
    }
}
